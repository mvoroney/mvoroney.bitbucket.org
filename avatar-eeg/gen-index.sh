#/usr/bin/env bash

base_dir=$(dirname `readlink $0 || ls $PWD/$0 2> /dev/null || ls $0 2> /dev/null`)
out_file="$base_dir/index.html"

cat > $out_file <<HTML_HEADER_BLOCK
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/xhtml;charset=UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=9"/>
<title>Avatar EEG Project: Source Documentation Index</title>
<link href="doxygen.css" rel="stylesheet" type="text/css" />
</head>
<body>
<h1>Avatar EEG Framework Source Documentation Index</h1>
<ul>
HTML_HEADER_BLOCK

for i in `ls -d -- $base_dir/* | sed "s@$base_dir/@@;s@/\./@/@g"`; do
    if [ -d $i ]; then
        echo "<li>version <a href=\"$i/index.html\">$i</a>.</li>" >> $out_file
    fi
done

cat >> $out_file <<HTML_FOOTER_BLOCK
</ul>
</body></html>
HTML_FOOTER_BLOCK

